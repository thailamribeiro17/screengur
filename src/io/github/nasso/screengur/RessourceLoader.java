package io.github.nasso.screengur;


import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class RessourceLoader {
	private static Map<String, BufferedImage> loadedImages = new HashMap<String, BufferedImage>();
	private static Map<String, Font> loadedFont = new HashMap<String, Font>();
	
	public static Image getImage(String ressourceName) {
		BufferedImage img = null;
		
		if(loadedImages.containsKey(ressourceName) && (img = loadedImages.get(ressourceName)) != null){
			img = loadedImages.get(ressourceName);
		}else{
			BufferedInputStream stream = new BufferedInputStream(Class.class.getResourceAsStream("/"+ressourceName));
			
			try {
				img = ImageIO.read(stream);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			loadedImages.put(ressourceName, img);
		}
		
		return img;
	}
	
	public static Font getFont(String ressourceName, int style, float size) {
		Font fnt = null;
		
		if(loadedFont.containsKey(ressourceName) && loadedFont.get(ressourceName) != null){
			fnt = loadedFont.get(ressourceName).deriveFont(style, size);
		}else{
			BufferedInputStream stream = new BufferedInputStream(RessourceLoader.class.getResourceAsStream("/"+ressourceName));
			
			try {
				fnt = Font.createFont(Font.TRUETYPE_FONT, stream).deriveFont(style, size);
			} catch (FontFormatException | IOException e) {
				e.printStackTrace();
			}
			
			loadedFont.put(ressourceName, fnt);
		}
		
		return fnt;
	}
}
