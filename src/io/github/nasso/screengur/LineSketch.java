package io.github.nasso.screengur;

import java.awt.Color;
import java.awt.Graphics2D;

public class LineSketch extends StrokedSketch {
	private int sx, sy, ex, ey;
	
	public LineSketch(int startx, int starty, int endx, int endy, float toolSize, Color toolColor) {
		super(toolSize, toolColor);
		
		sx = startx;
		sy = starty;
		ex = endx;
		ey = endy;
	}
	
	public void setStartX(int v) {
		sx = v;
	}
	
	public void setStartY(int v) {
		sy = v;
	}
	
	public void setEndX(int v) {
		ex = v;
	}
	
	public void setEndY(int v) {
		ey = v;
	}
	
	public void setValues(int sx, int sy, int ex, int ey) {
		this.sx = sx;
		this.sy = sy;
		this.ex = ex;
		this.ey = ey;
	}
	
	public void draw(Graphics2D g2d) {
		g2d.setStroke(this.stroke);
		g2d.setColor(this.color);
		
		g2d.drawLine(sx, sy, ex, ey);
	}

	public Sketch clone() {
		return new LineSketch(sx, sy, ex, ey, toolSize, color);
	}
}
