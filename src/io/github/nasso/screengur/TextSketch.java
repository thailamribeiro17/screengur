package io.github.nasso.screengur;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.apache.commons.lang3.StringUtils;

public class TextSketch extends Sketch implements KeyListener {
	private static final String LINE_SEP = "\n";
	private static final Stroke strk = new BasicStroke(1f);
	
	private Font font;
	
	private boolean editMode = true;
	private StringBuilder text = new StringBuilder();
	private int caretPos = 0;
	private int x, y;
	
	public TextSketch(int x, int y, float toolSize, Color toolColor) {
		super(toolSize, toolColor);
		
		this.x = x;
		this.y = y;
	}

	public void draw(Graphics2D g2d) {
		g2d.setFont(this.font);
		FontMetrics metrics = g2d.getFontMetrics();
		String[] lines = this.text.toString().split(LINE_SEP);
		
		int lineHeight = metrics.getHeight();
		int longestWidth = 0;
		int realLineCount = StringUtils.countMatches(this.text, LINE_SEP) + 1;
		
		for(int i = 0; i < lines.length; i++) {
			g2d.drawString(lines[i], x, y + lineHeight * (i + 1));
			
			longestWidth = Math.max(longestWidth, metrics.stringWidth(lines[i]));
		}
		
		if(editMode) {
			int caretLine = StringUtils.countMatches(this.text.substring(0, caretPos), LINE_SEP);
			int caretX = caretPos;
			
			for(int i = 0; i < caretLine; i++) {
				if(lines.length <= i) {
					break;
				} else {
					caretX -= lines[i].length() + 1;
				}
			}
			
			int x = this.x;
			int y = this.y + lineHeight * (caretLine + 1);
			
			if(caretLine < lines.length) {
				x += metrics.stringWidth(lines[caretLine].substring(0, caretX));
			}
			
			g2d.setColor(color);
			g2d.setStroke(strk);
			g2d.drawLine(x, y, x, y - metrics.getAscent());
			
			int xm = 8;
			int ym = 6;
			g2d.drawRect(this.x - xm, this.y + lineHeight - metrics.getAscent() - ym, longestWidth + xm * 2, lineHeight * realLineCount + ym * 2);
		}
	}
	
	public void setToolSize(float v) {
		super.setToolSize(v);
		this.font = new Font(Font.SANS_SERIF, Font.PLAIN, (int) v + 8);
	}

	public Sketch clone() {
		TextSketch clone = new TextSketch(x, y, toolSize, color);
		clone.text.append(text);
		clone.caretPos = this.caretPos;
		
		return clone;
	}

	public String getText() {
		return text.toString();
	}

	public void setText(String text) {
		this.text.setLength(0);
	}

	public int getCaretPos() {
		return caretPos;
	}

	public void setCaretPos(int caretPos) {
		this.caretPos = caretPos;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean v) {
		this.editMode = v;
	}
	
	public void keyTyped(KeyEvent e) {
		if(!editMode) {
			return;
		}
		
		 if(e.getKeyChar() == '\b') {
			if(text.length() > 0) {
				text.deleteCharAt(caretPos - 1);
				caretPos--;
			}
		} else if(!e.isControlDown()) {
			text.insert(caretPos, e.getKeyChar());
			caretPos++;
		}
	}

	public void keyPressed(KeyEvent e) {
		if(!editMode) {
			return;
		}
		
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			caretPos++;
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			caretPos--;
		}
		
		caretPos = Math.min(Math.max(0, caretPos), this.text.length());
	}

	public void keyReleased(KeyEvent e) {
		// Useless
	}
}
