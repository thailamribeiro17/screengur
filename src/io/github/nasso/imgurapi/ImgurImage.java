package io.github.nasso.imgurapi;

public class ImgurImage {
	private String id;
	private String title;
	private String description;
	private String deletehash;
	private String name;
	private String section;
	private String link;
	private String gifv;
	private String mp4;
	private String type;
	private String vote;
	
	private int datetime;
	private int width;
	private int height;
	private int size;
	private int views;
	private int bandwidth;
	private int mp4_size;
	
	private boolean animated;
	private boolean looping;
	private boolean favorite;
	private boolean nsfw;
	private boolean in_gallery;
	
	public String getID() {
		return id;
	}
	
	public void setID(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeleteHash() {
		return deletehash;
	}

	public void setDeleteHash(String deletehash) {
		this.deletehash = deletehash;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getGifv() {
		return gifv;
	}

	public void setGifv(String gifv) {
		this.gifv = gifv;
	}

	public String getMP4() {
		return mp4;
	}

	public void setMP4(String mp4) {
		this.mp4 = mp4;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVote() {
		return vote;
	}

	public void setVote(String vote) {
		this.vote = vote;
	}

	public int getDateTime() {
		return datetime;
	}

	public void setDateTime(int datetime) {
		this.datetime = datetime;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public int getMP4Size() {
		return mp4_size;
	}

	public void setMP4Size(int mp4_size) {
		this.mp4_size = mp4_size;
	}

	public boolean isAnimated() {
		return animated;
	}

	public void setAnimated(boolean animated) {
		this.animated = animated;
	}

	public boolean isLooping() {
		return looping;
	}

	public void setLooping(boolean looping) {
		this.looping = looping;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public boolean isNSFW() {
		return nsfw;
	}

	public void setNSFW(boolean nsfw) {
		this.nsfw = nsfw;
	}

	public boolean isInGallery() {
		return in_gallery;
	}

	public void setInGallery(boolean in_gallery) {
		this.in_gallery = in_gallery;
	}
}
