package io.github.nasso.utils;

import java.awt.Rectangle;

public class Utils {
	private Utils(){}
	
	public static Rectangle constructRectangle(int ax, int ay, int bx, int by, Rectangle dest){
		int x = Math.min(ax, bx);
		int y = Math.min(ay, by);
		int width = Math.max(ax, bx) - x;
		int height = Math.max(ay, by) - y;
		
		dest.setBounds(x, y, width, height);
		return dest;
	}
}
